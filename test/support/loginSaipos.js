const { default: axios } = require("axios");
const credentials = require("../fixtures/credentials.json")
const url = 'http://localhost:3000/';

let token;


async function doLogin() { 
    const res = await axios.post(url+'v1/users/login?include=user',
    {
        email: credentials.email,
        force: true,
        password: credentials.password
    });
        return res.data.id;
}
        
module.exports = {
    doLogin  
};